import { useEffect, useState } from "react";
import useSWR from "swr";
import { fetcher } from "./fetcher";
import { useRouter } from "next/router";
import { User } from "@prisma/client";

export function useLocalStorage<T>(key: string) {
    const [value, setValue] = useState<T | null>(null);
    useEffect(() => {
        const stored = localStorage.getItem(key);
        setValue(stored ? JSON.parse(stored) : null);
    }, [key]);

    useEffect(() => {
        localStorage.setItem(key, JSON.stringify(value));
    }, [key, value]);

    return [value, setValue] as const;
}

export const useMe = () => {
    const { data, error, isLoading } = useSWR(`/api/users/me`, fetcher);
    console.log(data);
    const user = data as User | undefined;
    return { user, error, isLoading };
};

export const useRedirectIfUnauthed = () => {
    const router = useRouter();
    const [accessToken, setAccessToken] =
        useLocalStorage<string>("accessToken");

    useEffect(() => {
        if (accessToken === null) {
            router.push("/");
        }
    }, [router, accessToken]);
};
