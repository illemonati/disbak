import _ from "lodash";

export const toCamelCase = (obj: object) => {
    return _.mapKeys(obj, (v, k) => _.camelCase(k));
};
