import axios from "axios";

export const onboard = async (code: string) => {
    const resp = await axios.post("/api/onboarding", {
        code,
    });

    const accessToken = resp.data["accessToken"] as string;
    return accessToken;
};
