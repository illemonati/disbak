import axios from "axios";

export const fetcher = (url: string) =>
    axios
        .get(url, {
            headers: {
                Authorization: JSON.parse(
                    localStorage.getItem("accessToken") as string
                ),
            },
        })
        .then((res) => res.data);
