import { createTheme } from "@mui/material/styles";

export const theme = createTheme({
    palette: {
        mode: "dark",
        primary: {
            main: "#5865f2",
        },
        secondary: {
            main: "#ffffff",
        },
        background: {
            paper: "#36393f",
            default: "#202225",
        },
        text: {
            secondary: "rgba(255, 255, 255, 0.7)",
        },
    },
    typography: {
        fontFamily: "Open Sans",
    },
});
