import {
    discordApiEndpoint,
    discordCdnEndpoint,
    discordRedirectUrl,
} from "@/configs/urls";
import { AccessToken } from "@prisma/client";
import axios from "axios";
import qs from "qs";

interface MeResp {
    id: string;
    username: string;
    discriminator: string;
    avatar: string;
    bot?: boolean;
    system?: boolean;
    mfa_enabled?: boolean;
    locale?: string;
    verified: boolean;
    email: string;
    flags: number;
    banner: string;
    accent_color: number;
    premium_type: number;
    public_flags: number;
}

export const getMe = async (token: AccessToken): Promise<MeResp> => {
    console.log(token);
    const resp = await axios.get(`${discordApiEndpoint}/users/@me`, {
        headers: {
            Authorization: `${token.tokenType} ${token.accessToken}`,
        },
    });

    return resp.data;
};

interface AccessTokenResp {
    access_token: string;
    token_type: string;
    expires_in: number;
    refresh_token: string;
    scope: string;
}

export const getAccessToken = async (
    code: string
): Promise<AccessTokenResp> => {
    const resp = await axios.post(
        `${discordApiEndpoint}/oauth2/token`,
        qs.stringify({
            client_id: process.env.CLIENT_ID,
            client_secret: process.env.CLIENT_SECRET,
            grant_type: "authorization_code",
            code: code,
            redirect_uri: discordRedirectUrl,
        }),
        {
            headers: { "content-type": "application/x-www-form-urlencoded" },
        }
    );
    return resp.data;
};

export const parseAuthorization = (authorization: string) => {
    return authorization.split(" ")[1];
};

export const getUserBannerURL = (userId: string, banner: string) => {
    return `${discordCdnEndpoint}/banners/${userId}/${banner}.png?size=2048`;
};

export const getUserAvatarURL = (
    userId: string,
    discrim: number,
    avatar?: string | null
) => {
    return avatar
        ? `${discordCdnEndpoint}/avatars/${userId}/${avatar}`
        : `${discordCdnEndpoint}/embed/avatars/${discrim % 5}.png`;
};

export const parseAccentColor = (accentColor: number) => {
    return `#${accentColor.toString(16)}`;
};
