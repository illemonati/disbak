import Head from "next/head";
import Image from "next/image";
import styles from "@/styles/Home.module.css";
import { Button, Container, Stack, Typography } from "@mui/material";
import DiscordIcon from "@/components/utils/DiscordIcon";
import DispackLogo from "@/public/assets/images/disbak-logo.png";
import Link from "next/link";
import { discordAuthorizationUrl } from "@/configs/urls";

export default function Home() {
    return (
        <>
            <Head>
                <title>Disbak</title>
            </Head>

            <Stack
                justifyContent="center"
                alignItems="center"
                gap={2}
                style={{ width: "100%", height: "80%" }}
            >
                {/* <Typography variant="h3">DISBAK</Typography> */}
                <Image
                    src={DispackLogo}
                    style={{ height: "3rem", width: "auto" }}
                    alt="DISBAK"
                />
                <Typography variant="h5">Discord Backup Service</Typography>
                <Link
                    href={discordAuthorizationUrl}
                    style={{ textDecoration: "none" }}
                >
                    <Button
                        style={{ marginTop: "2rem" }}
                        variant="contained"
                        size="large"
                        startIcon={<DiscordIcon height="100%" />}
                    >
                        Login With Discord
                    </Button>
                </Link>
            </Stack>
        </>
    );
}
