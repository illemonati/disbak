import "@/styles/globals.css";
import { theme } from "@/utils/theme";
import "@fontsource/open-sans";
import { ThemeProvider } from "@mui/material";

import type { AppProps } from "next/app";
import Head from "next/head";

export default function App({ Component, pageProps }: AppProps) {
    return (
        <ThemeProvider theme={theme}>
            <Head>
                <meta name="description" content="Discord Backup Service" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1"
                />
                <meta name="theme-color" content={theme.palette.primary.main} />
                <link rel="icon" href="/favicon.ico" />
                <link
                    rel="icon"
                    type="image/png"
                    href="/assets/images/disbak-logo-margin.png"
                ></link>
                <meta
                    property="og:image"
                    content="/assets/images/disbak-logo-margin.png"
                />
            </Head>
            <Component {...pageProps} />
        </ThemeProvider>
    );
}
