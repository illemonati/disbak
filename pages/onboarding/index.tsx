import Head from "next/head";
import {
    Box,
    CircularProgress,
    Container,
    Stack,
    Typography,
} from "@mui/material";
import { useEffect } from "react";
import { onboard } from "@/utils/posts";
import { useRouter } from "next/router";
import { useLocalStorage } from "@/utils/client-side-data";

export default function Onboarding() {
    const router = useRouter();
    const { code } = router.query;

    const [accessToken, setAccessToken] =
        useLocalStorage<string>("accessToken");

    useEffect(() => {
        if (!code) return;
        onboard(code as string).then((token) => {
            console.log(token);
            setAccessToken(token);
        });
    }, [code, setAccessToken]);

    useEffect(() => {
        if (accessToken && accessToken.length > 0) {
            // mandatory wait (will remove later)
            setTimeout(() => {
                router.push("/onboarding/confirm-user");
            }, 3000);
        }
    }, [accessToken, router]);

    return (
        <>
            <Head>
                <title>Onboarding | Disbak</title>
            </Head>

            <Container style={{ height: "80%" }} maxWidth="md">
                <Stack
                    justifyContent="center"
                    alignItems="center"
                    gap={6}
                    style={{ width: "100%", height: "100%" }}
                >
                    {code ? (
                        <>
                            <Typography variant="h4">
                                Welcome to Disbak
                            </Typography>
                            <Box sx={{ display: "flex" }}>
                                <CircularProgress
                                    style={{
                                        width: "3.3rem",
                                        height: "3.3rem",
                                    }}
                                />
                            </Box>
                        </>
                    ) : (
                        <Typography variant="h4">
                            Error, Please Try Again
                        </Typography>
                    )}
                </Stack>
            </Container>
        </>
    );
}
