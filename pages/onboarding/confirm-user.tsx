import {
    useLocalStorage,
    useMe,
    useRedirectIfUnauthed,
} from "@/utils/client-side-data";
import {
    getUserAvatarURL,
    getUserBannerURL,
    parseAccentColor,
} from "@/utils/userinfo";
import {
    Avatar,
    Box,
    Button,
    Container,
    Paper,
    Stack,
    Typography,
} from "@mui/material";
import Head from "next/head";
import Image from "next/image";

export default function ConfirmUser() {
    const { user, error, isLoading } = useMe();

    console.log(user, error, isLoading);

    if (isLoading) return;
    if (error || !user) return;

    console.log(user);

    return (
        <>
            <Head>
                <title>Confirm User | Disbak</title>
            </Head>

            <Container
                style={{
                    height: "100%",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                }}
                maxWidth="sm"
            >
                <Paper style={{ width: "100%", height: "80%" }}>
                    <Stack style={{ display: "flex", height: "100%" }}>
                        {user.banner ? (
                            <Image
                                src={getUserBannerURL(user.id, user.banner)}
                                alt="banner"
                                width={2048}
                                height={2048}
                                style={{
                                    width: "100%",
                                    height: "20%",
                                    objectFit: "cover",
                                    borderTopLeftRadius: 4,
                                    borderTopRightRadius: 4,
                                    marginBottom: "-15%",
                                }}
                            />
                        ) : (
                            <Box
                                style={{
                                    width: "100%",
                                    height: "20%",
                                    background: user.accentColor
                                        ? parseAccentColor(user.accentColor)
                                        : "black",
                                    objectFit: "cover",
                                    borderTopLeftRadius: 4,
                                    borderTopRightRadius: 4,
                                    marginBottom: "-15%",
                                }}
                            />
                        )}

                        <Container maxWidth="sm">
                            <Avatar
                                alt="avatar"
                                src={getUserAvatarURL(
                                    user.id,
                                    parseInt(user.discriminator),
                                    user.avatar
                                )}
                                style={{
                                    height: "auto",
                                    width: "30%",
                                    marginBottom: "10%",
                                }}
                            />
                            <Paper variant="outlined">
                                <Container
                                    maxWidth="sm"
                                    style={{
                                        height: "100%",
                                    }}
                                >
                                    <Stack gap="1rem" paddingY="1rem">
                                        <Typography variant="h5">
                                            {user.username}#{user.discriminator}
                                        </Typography>
                                        <Line />
                                        <Typography variant="button">
                                            Disclaimer
                                        </Typography>
                                        <Typography variant="body2">
                                            By clicking on continue, I confirm
                                            this account is rightfully mine.
                                            Disbak is not responsible for any
                                            unauthorized accesses and
                                            activities.
                                        </Typography>
                                        <Typography variant="body2">
                                            Data recorded will not be accessed
                                            except by the user, deletion of all
                                            data can be requested from within
                                            the app or by email at
                                            support@disbak.com
                                        </Typography>
                                    </Stack>
                                </Container>
                            </Paper>
                        </Container>
                        <Box
                            style={{
                                width: "100%",
                                flex: 1,
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center",
                            }}
                        >
                            <Button variant="contained" size="large">
                                I agree and Continue
                            </Button>
                        </Box>
                    </Stack>
                </Paper>
            </Container>
        </>
    );
}

const Line = () => {
    return (
        <div
            style={{
                borderBottom: "1px solid var(--tertiary)",
            }}
        ></div>
    );
};
