// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { prisma } from "@/prisma";
import { toCamelCase } from "@/utils/case-conversion";
import { getAccessToken, getMe } from "@/utils/userinfo";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const { code } = req.body;

    if (!code) return res.status(400).json({ error: "invalid code" });

    const accessToken = await getAccessToken(code);

    const databaseAccessToken = await prisma.accessToken.upsert({
        where: {
            accessToken: accessToken.access_token,
        },
        create: {
            accessToken: accessToken.access_token,
            tokenType: accessToken.token_type,
            refreshToken: accessToken.refresh_token,
            expiresIn: accessToken.expires_in,
            scope: accessToken.scope,
        },
        update: {
            refreshToken: accessToken.refresh_token,
            expiresIn: accessToken.expires_in,
            scope: accessToken.scope,
        },
    });

    const me = await getMe(databaseAccessToken);
    const databaseUser = await prisma.user.upsert({
        where: {
            id: me.id,
        },
        create: {
            id: me.id,
            username: me.username,
            discriminator: me.discriminator,
            avatar: me.avatar,
            bot: me.bot,
            system: me.system,
            mfaEnabled: me.mfa_enabled,
            banner: me.banner,
            accentColor: me.accent_color,
            locale: me.locale,
            verified: me.verified,
            email: me.email,
            flags: me.flags,
            premiumType: me.premium_type,
            publicFlags: me.public_flags,

            accessToken: {
                connect: {
                    accessToken: databaseAccessToken.accessToken,
                },
            },
        },
        update: {
            username: me.username,
            discriminator: me.discriminator,
            avatar: me.avatar,
            bot: me.bot,
            system: me.system,
            mfaEnabled: me.mfa_enabled,
            banner: me.banner,
            accentColor: me.accent_color,
            locale: me.locale,
            verified: me.verified,
            email: me.email,
            flags: me.flags,
            premiumType: me.premium_type,
            publicFlags: me.public_flags,

            accessToken: {
                connect: {
                    accessToken: databaseAccessToken.accessToken,
                },
            },
        },
    });

    res.status(200).json({
        accessToken: accessToken.access_token,
    });
}
