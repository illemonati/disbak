// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { prisma } from "@/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const { authorization } = req.headers;

    if (!authorization) return res.status(400).json({ error: "invalid token" });

    const user = await prisma.user.findUnique({
        where: {
            accessTokenAccessToken: authorization as string,
        },
    });

    if (!user) return res.status(401).json({ error: "unauthorized" });

    return res.status(200).json(user);
}
