export const discordAuthorizationUrl =
    process.env.NODE_ENV === "development"
        ? "https://discord.com/api/oauth2/authorize?client_id=1087180231334445207&redirect_uri=http%3A%2F%2Flocalhost%3A26548%2Fonboarding&response_type=code&scope=identify%20messages.read%20guilds%20guilds.members.read"
        : "https://discord.com/api/oauth2/authorize?client_id=1087180231334445207&redirect_uri=https%3A%2F%2Fdisbak.com%2Fonboarding&response_type=code&scope=identify";

export const discordRedirectUrl =
    process.env.NODE_ENV === "development"
        ? "http://localhost:26548/onboarding"
        : "https://disbak.com/onboarding";

export const discordApiEndpoint = "https://discord.com/api/v10";
export const discordCdnEndpoint = "https://cdn.discordapp.com";
